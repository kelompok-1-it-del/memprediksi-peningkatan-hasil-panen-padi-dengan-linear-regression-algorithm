import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#read data from file
dataset = pd.read_csv('normalizedata.csv')
X=dataset.iloc[:,2:3].values
Y=dataset.iloc[:, 3].values

#splitting dataset into trainingset and test set
from sklearn.cross_validation import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=0)


#Training Model
from sklearn import linear_model
linear_regressor = linear_model.LinearRegression()
linear_regressor.fit(X_train, Y_train)
Y_train_pred = linear_regressor.predict(X_train)

plt.figure()
plt.scatter(X_train, Y_train, color='green')
plt.plot(X_train, Y_train_pred, color='red', linewidth=1)
plt.title=('Training Data')
plt.show()

#Testing Model
linear_regressor.fit(X_test, Y_test)
Y_test_pred = linear_regressor.predict(X_test)

plt.figure()
plt.scatter(X_test, Y_test, color='blue')
plt.plot(X_test, Y_test_pred, color='yellow', linewidth=1)
plt.title=('Testing Data')
plt.show()

