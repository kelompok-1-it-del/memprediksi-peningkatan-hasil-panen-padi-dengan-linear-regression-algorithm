# Memprediksi Peningkatan Hasil Panen Padi di Indonesia dengan Menerapkan Linear Regression Algorithm 


## Quick start

Preparation:
Install Anaconda2
Install Python2
Install Lybrary yang dibutuhkan


# Cara menjalankan program:
1. Buka aplikasi Anaconda Navigator
2. Pilih Spyder  yang terdapat pada halaman awal Anaconda Navigator
3. Tekan tombol Launch. Spyder notebook akan ditampilkan pada web browser.
4. Untuk mengupload kode program, tekan tombol upload yang terdapat pada halaman Home Spyder.
5. Pilih file yang terdapat pada file Program
6. Tekan tombol upload
7. File berhasil di load, pilih kode program untuk membukanya pada tab baru
8. Buatlah file baru dengan cara menekan tombol New yang terdapat pada halaman Home
9. Salin setiap kode program ke halaman Spyder
10. Untuk menjalankan kode program satu kolom dapat dilakukan dengan menekan tombol Shift+Enter atau run cell
11. Setelah semua kolom disalin, jalankan program dengan memilih menu Kernel-> Restart & Run all
12. Tunggu beberapa saat program berjalan untuk menampilkan hasil

# cara menjalankan GUI 
1. Buka Aplikasi Anaconda dengan me launch Spyder
2. Pilih kode program main.py
3. Run kode program dan pilih configuration file
4. GUI akan muncul setelah cmd muncul
5. Interface akan muncul, lalu pilih grafik data yang mau dilihat, contoh data tarining
6. Pilih result maka grafik training data akan muncuk pada GUI anda.

Terima kasih.