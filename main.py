import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from tkinter import *
import sys
import os.path
from copy import deepcopy
node=None
algo =None
push_Button=None
root=None
counter = -1
#read data from file
dataset = pd.read_csv('normalizedata.csv')
X=dataset.iloc[:,2:3].values
Y=dataset.iloc[:, 3].values

#splitting dataset into trainingset and test set
from sklearn.cross_validation import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=0)


#Training Model
from sklearn import linear_model
def data_train():
    linear_regressor = linear_model.LinearRegression()
    linear_regressor.fit(X_train, Y_train)
    linear_regressor.fit(X_test, Y_test)
    Y_train_pred = linear_regressor.predict(X_train)
    

#Testing Model
def data_set():  
    linear_regressor = linear_model.LinearRegression()
    linear_regressor.fit(X_train, Y_train)
    linear_regressor.fit(X_test, Y_test)
    Y_test_pred = linear_regressor.predict(X_test)
def display_graphics(data_train):
    linear_regressor = linear_model.LinearRegression()
    linear_regressor.fit(X_train, Y_train)
    linear_regressor.fit(X_test, Y_test)
    Y_train_pred = linear_regressor.predict(X_train)
    plt.figure()
    plt.scatter(X_train, Y_train, color='green')
    plt.plot(X_train, Y_train_pred, color='red', linewidth=1)
    plt.title=('Training Data')
    plt.show()
    
    
def display_graphic(data_set):
    linear_regressor = linear_model.LinearRegression()
    linear_regressor.fit(X_train, Y_train)
    linear_regressor.fit(X_test, Y_test)
    Y_test_pred = linear_regressor.predict(X_test)
    plt.figure()
    plt.scatter(X_test, Y_test, color='blue')
    plt.plot(X_test, Y_test_pred, color='yellow', linewidth=1)
    plt.title=('Testing Data')
    plt.show()
def on_click():
    global push_Button, algo,counter,nodes,nud
    
    if "data set" == algo.get():
        nud=data_set()
        display_graphic(data_set)
        push_Button.config()
    elif "data train" == algo.get():
        nodes=data_train()
        display_graphics(data_train)
        push_Button.config()
    
    
def main():
    global algo,push_Button
    root = Tk()
    root.title("Prediksi hasil Padi")
    root.geometry("400x300")
    algo=StringVar(root)
    algorithm_menu= OptionMenu(
            root,
            algo,"data set","data train")
    Label(root,text="Prediksi Padi").pack()
    algorithm_menu.pack()
    frame1=Frame(root)
    push_Button= Button(
            frame1,
            width=5,
            height=1,
            text="Result",
            command=on_click,
            padx=2,
            pady=2,
            relief=GROOVE)
    push_Button.pack(side=LEFT)
    frame1.pack(side=BOTTOM)
    root.mainloop()
    
if __name__ == "__main__":
    main()